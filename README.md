### Dominio Estudio: 
##### Prueba Desarrollador Frontend

Se añade proyecto desarrollado en Vuejs para la prueba de Desarrollador Front-end:

La dirección principal para la ejecución correcta del proyecto es:

`localhost:8080/#/1`

Proyecto Principal: `dominio/`


| Desarrollador | Correo                   |
| ------------- | ------------------------------ |
| Cristhian M. Yara P.  | cmyarap@correo.udistrital.edu.co  |